function loadDoc1(){
    var nama;
    var lahir;
    var alamat;
    var poli;
    var telepon;
    var konf;

    nama=prompt("Masukan nama pasien ");
    lahir=prompt("Masukan tanggal lahir pasien ");
    alamat=prompt("Masukan alamat pasien ");
    telepon=prompt("Masukan No telepon yang bisa dihubungi");
    poli=prompt("Pilih Poli (1.Jantung / 2.Paru / 3.Bedah / 4.Gizi)");

    konf=0;

    if(poli==1){
        poli="Poli Jantung";
        konf=1;
    }
    else if(poli==2){
        poli="Poli Paru";
        konf=1;
    }
    else if(poli==3){
        poli="Poli Bedah";
        konf=1;
    }
    else if(poli==4){
        poli="Poli Gizi";
        konf=1;
    }
    else{
        alert("Inputan tidak sesuai,silahkan input kembali");
        document.location.href="index.html"
    }

    if(konf==1){
        alert("Nama : "+nama + "\nTanggal Lahir : "+lahir+"\nAlamat : "+alamat+"\nTelepon : "+telepon+"\nPoli : "+poli+("\n\nPasien dengan Data diri diatas telah berhasil didaftarkan sebagai pasien RSUD Ungaran\nSilahkan Konfirmasi pembayaran di RSUD Ungaran"));
    }
}

function loadTable1(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function(){
        if(this.readyState == 4 && this.status == 200){
            document.getElementById("tabelanak").innerHTML=this.responseText;
        }
    };
    xhttp.open("GET","childtable.html",true);
    xhttp.send();
}

function loadTable2(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function(){
        if(this.readyState == 4 && this.status == 200){
            document.getElementById("tabelumum").innerHTML=this.responseText;
        }
    };
    xhttp.open("GET","normaltable.html",true);
    xhttp.send();
}

function loadTable3(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function(){
        if(this.readyState == 4 && this.status == 200){
            document.getElementById("tabellansia").innerHTML=this.responseText;
        }
    };
    xhttp.open("GET","oldtable.html",true);
    xhttp.send();
}

function closeDoc1(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function(){
        if(this.readyState == 4 && this.status == 200){
            document.getElementById("tabelanak").innerHTML=this.responseText;
        }
    };
    xhttp.open("GET","blank.html",true);
    xhttp.send();
}

function closeDoc2(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function(){
        if(this.readyState == 4 && this.status == 200){
            document.getElementById("tabelumum").innerHTML=this.responseText;
        }
    };
    xhttp.open("GET","blank.html",true);
    xhttp.send();
}

function closeDoc3(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function(){
        if(this.readyState == 4 && this.status == 200){
            document.getElementById("tabellansia").innerHTML=this.responseText;
        }
    };
    xhttp.open("GET","blank.html",true);
    xhttp.send();
}



var txt = '{"name":"Dr. Nafrialdi, Phd, SpPD", "specialist":"Spesialis Penyakit Dalam", "education":"Pendidikan Spesialis Penyakit Dalam Universitas Indonesia","award":"Narasumber Kuliah Terbaik FKUI ( 2015 )"}';
var obj = JSON.parse(txt);
document.getElementById("jsonname").innerHTML = obj.name;
document.getElementById("jsonspecial").innerHTML = obj.specialist;
document.getElementById("jsoneducation").innerHTML = obj.education;
document.getElementById("jsonaward").innerHTML = obj.award;
